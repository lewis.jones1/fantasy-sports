# Fantasy Sports App

This is an app for simulating Fantasy F1 and Fantasy Premier League (football).
It has been written as part of a training exercise to improve my knowledge of OOP and software design.

## Examples of Object-Oriented Programming Principles
### Encapsulation

All classes have been declared following the principle of encapsulation. All methods and attributes
of a class are grouped together and all variables are private and accessible using getters and setters.

A good example of encapsulation is demonstrated in the `FootballPlayer` class, which contains sensitive
information about a player including their name and lists of all of their goals, saves, cards, etc.
These need to be private so other classes or users cannot interfere with this data, but getters and
setters ensure that the data is still accessible if required.

For example, there are no public methods to update the Goals, Saves, Cards and Games lists,
because these will only be able to be updated by using the methods from the `Scoreable` interface.
There is a setter for `position`, since a players position may need to be changed in the future.

### Inheritance

The scoring events are good examples of inheritance (`Goal`, `Save`, `Card`).
Each of these classes inherit from the `TimedEvent` class, which itself inherits from the `Event`
class. Inheriting from the `Event` class provides the child classes with the `player` and the `fixture`
associated with the event, whilst `TimedEvent` provides the functionality to record the time the event
occurred. This is important, because `PlayerGametime` does not have a time associated with it,
but it does need access to `player` and `fixture`.

It is logical that these classes all inherit from `Event`, since they all occur in a football game
and they have various points attached to them in the fantasy football game.

Other examples of inheritance include the `FootballPlayer` class inheriting from the `Member` class.
When fantasy F1 is added to this project, the `F1Driver` can also inherit from the `Member` class,
as `Driver` will require the same methods and attributes as `FootballPlayer` from the `Member` class.
This shows how inheritance can be used to group attributes of different, but similar, classes together
and also improves reusability because each class does not require similar methods and attributes to be
declared each time (as they are all contained within the parent class).

### Polymorphism

One example of polymorphism is seen in `Calculable:4-5`.

```java
public interface Calculable {
  double calculateGameweekPoints();
  double calculateGameweekPoints(int gameweekNumber);
}
```

Here, the same method `calculateGameweekPoints` is declared twice - once without parameters, and
once with a `gameweekNumber` parameter. This allows the user to calculate the current gameweek points
using the first method, but they can also get the gameweek points of a specific gameweek by passing
the gameweek number as a parameter. This is an example of **method overloading**, or **compile time** 
polymorphism.

**Runtime polymorphism** would be where a method is **overridden**, where a child class implements
a method that it has inherited from the parent class in a different way. Since all methods in this
project are contained in interfaces, there are currently no examples of this polymorphism, although
this will be added at a later date if the opportunity arises.

### Abstraction

I have used abstraction in a number of ways. Firstly, I have abstracted all methods into interfaces,
e.g. `Calculable` for calculating a players gameweek points, and `Scoreable` for adding goals and
other events to a `FootballPlayer` object. Interfaces are classes that contain only abstract methods,
so when a class `implements` an interface, all methods in the interface **must** be implemented in
the concrete class.

I have also used abstract classes, e.g. `Event`, `TimedEvent`, `Member`. These classes cannot be
instantiated themselves, but they are inherited by concrete classes (`Goal`, `FootballPlayer`).
The abstract classes contain methods that are not abstract, as each inheriting class should be able
to use these methods, but these classes could also contain abstract classes which would be implemented
in the child class.
