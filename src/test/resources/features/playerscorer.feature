Feature: Player Scorer

Scenario: Player
  Given the tests are set up
  When salah starts at 0 minutes in fixture1
  And salah ends at 90 minutes in fixture1
  And salah scores 1 goal in fixture1
  And salah gets a yellow card in fixture1
  Then salah should get 5 points in fixture1
