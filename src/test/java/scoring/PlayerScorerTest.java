package scoring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import fixtures.FootballFixture;
import fixtures.FootballMatchScoring;
import fixtures.Gameweek;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pointsscoring.football.CardType;
import sportsteam.FootballPlayer;
import sportsteam.Position;
import sportsteam.Team;

public class PlayerScorerTest {

  Team liverpool;
  Team arsenal;
  Team manCity;
  Team manUtd;

  FootballFixture fixture;
  FootballFixture fixture1;
  FootballFixture fixture2;

  Gameweek gameweek1;
  Gameweek gameweek2;

  FootballPlayer player;
  FootballPlayer player2;

  FootballMatchScoring matchScoring;
  PlayerScorer playerScorer;

  @BeforeEach
  public void init() {
    fixture = new FootballFixture(1, null, null, null);

    fixture1 = new FootballFixture(1, null, liverpool, arsenal);
    fixture2 = new FootballFixture(2, null, liverpool, manCity);

    gameweek1 = new Gameweek(1, 1, null, null);
    gameweek2 = new Gameweek(2, 2, null, null);

    liverpool = new Team("LPL", "Liverpool FC");
    arsenal = new Team("ARS", "Arsenal");
    manCity = new Team("MC", "Manchester City");
    manUtd = new Team("MU", "Manchester United");

    player = new FootballPlayer(null, null, liverpool, Position.FORWARD);
    player2 = new FootballPlayer(null, null, null, Position.MIDFIELDER);

    matchScoring = new FootballMatchScoring();
    playerScorer = new PlayerScorer();

    gameweek1.addFixtureToGameweek(fixture1);
    gameweek2.addFixtureToGameweek(fixture2);
  }

  @Test
  public void testCalculateTotalPoints90Minutes() {
    matchScoring.addPlayerStartTime(0, player, fixture);
    matchScoring.addPlayerEndTime(90, player, fixture);

    assertEquals(playerScorer.calculateTotalPoints(player), 2);
  }

  @Test
  public void testCalculateTotalPoints45Minutes() {
    matchScoring.addPlayerStartTime(0, player, fixture);
    matchScoring.addPlayerEndTime(45, player, fixture);

    assertEquals(playerScorer.calculateTotalPoints(player), 1);
  }

  @Test
  public void testCalculateTotalPoints90Minutes2Goals() {
    matchScoring.addPlayerStartTime(0, player, fixture);
    matchScoring.addPlayerEndTime(90, player, fixture);

    matchScoring.recordGoal(20, player, null, false, fixture);
    matchScoring.recordGoal(67, player, null, false, fixture);

    assertEquals(playerScorer.calculateTotalPoints(player), 10);
  }

  @Test
  public void testCalculateTotalPoints90Minutes2Goals1Yellow() {
    matchScoring.addPlayerStartTime(0, player, fixture);
    matchScoring.addPlayerEndTime(90, player, fixture);

    matchScoring.recordGoal(20, player, null, false, fixture);
    matchScoring.recordGoal(67, player, null, false, fixture);
    matchScoring.recordCard(52, CardType.YELLOW, player, fixture);

    assertEquals(playerScorer.calculateTotalPoints(player), 9);
  }

  @Test
  public void testCalculateTotalPoints90Minutes2Goals1Red() {
    matchScoring.addPlayerStartTime(0, player, fixture);
    matchScoring.addPlayerEndTime(90, player, fixture);

    matchScoring.recordGoal(20, player, null, false, fixture);
    matchScoring.recordGoal(67, player, null, false, fixture);
    matchScoring.recordCard(90, CardType.RED, player, fixture);

    assertEquals(playerScorer.calculateTotalPoints(player), 7);
  }

  @Test
  public void testCalculateTotalPoints90Minutes2Goals2Yellows() {
    matchScoring.addPlayerStartTime(0, player, fixture);
    matchScoring.addPlayerEndTime(90, player, fixture);

    matchScoring.recordGoal(20, player, null, false, fixture);
    matchScoring.recordGoal(67, player, null, false, fixture);
    matchScoring.recordCard(85, CardType.YELLOW, player, fixture);
    matchScoring.recordCard(90, CardType.YELLOW, player, fixture);

    assertEquals(playerScorer.calculateTotalPoints(player), 6);
  }

  @Test
  public void testCalculateTotalPoints90Minutes1Goal2Assists() {
    matchScoring.addPlayerStartTime(0, player, fixture);
    matchScoring.addPlayerEndTime(90, player, fixture);

    matchScoring.recordGoal(20, player, null, false, fixture);
    matchScoring.recordGoal(67, player2, player, false, fixture);
    matchScoring.recordGoal(75, player2, player, false, fixture);

    assertEquals(playerScorer.calculateTotalPoints(player), 12);
  }

  @Test
  public void testCalculateTotalPoints90Minutes3Saves() {
    FootballPlayer goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);

    matchScoring.addPlayerStartTime(0, goalkeeper, fixture);
    matchScoring.addPlayerEndTime(90, goalkeeper, fixture);

    matchScoring.recordSave(21, goalkeeper, fixture);
    matchScoring.recordSave(42, goalkeeper, fixture);
    matchScoring.recordSave(69, goalkeeper, fixture);

    assertEquals(playerScorer.calculateTotalPoints(goalkeeper), 3);
  }

  @Test
  public void testGetScoringEventsForAGameweek() {
    // fixture 1
    matchScoring.addPlayerStartTime(0, player, fixture1);
    matchScoring.addPlayerEndTime(90, player, fixture1);

    matchScoring.recordGoal(20, player, null, false, fixture1);
    matchScoring.recordCard(78, CardType.YELLOW, player, fixture1);

    assertEquals(playerScorer.calculateGameweekPoints(player, gameweek1), 5);
  }
}
