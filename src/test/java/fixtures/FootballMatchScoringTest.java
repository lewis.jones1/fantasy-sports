package fixtures;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pointsscoring.football.Card;
import pointsscoring.football.CardType;
import pointsscoring.football.PlayerGametime;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class FootballMatchScoringTest {
  FootballFixture fixture1;
  FootballFixture fixture2;
  FootballPlayer player1;
  FootballPlayer player2;
  FootballMatchScoring matchScoring;

  @BeforeEach
  public void init() {
    fixture1 = new FootballFixture(1, null, null, null);
    fixture2 = new FootballFixture(2, null, null, null);
    player1 = new FootballPlayer(null, null, null, null);
    player2 = new FootballPlayer(null, null, null, null);
    matchScoring = new FootballMatchScoring();
  }

  @Test
  public void testRecordGoalAddsGoalForPlayer() {
    matchScoring.recordGoal(2, player1, null, false, fixture1);
    assertEquals(player1.getGoals().size(), 1);
  }

  @Test
  public void testRecordGoalAddsGoalAndAssistForBothPlayers() {
    matchScoring.recordGoal(2, player1, player2, false, fixture1);
    assertEquals(player1.getGoals().size(), 1);
    assertEquals(player2.getAssists().size(), 1);
  }

  @Test
  public void testRecordOwnGoalAddsAnOwnGoalForScorer() {
    matchScoring.recordGoal(15, player1, null, true, fixture1);
    assertEquals(player1.getGoals().size(), 0);
    assertEquals(player1.getOwnGoals().size(), 1);
  }

  @Test
  public void testRecordGoalRecordsAssist() {
    matchScoring.recordGoal(20, player1, player2, true, fixture1);
    assertEquals(player1.getGoals().size(), 0);
    assertEquals(player1.getOwnGoals().size(), 1);
    assertEquals(player2.getAssists().size(), 1);
  }

  @Test
  public void testRecordCardRecordsYellowCardCorrectly() {
    matchScoring.recordCard(80, CardType.YELLOW, player1, fixture1);
    List<Card> cards = player1.getCards();

    assertEquals(cards.size(), 1);
    assertEquals(cards.get(0).getCardType(), CardType.YELLOW);
  }

  @Test
  public void testRecordCardRecordsRedCardCorrectly() {
    matchScoring.addPlayerStartTime(0, player1, fixture1);
    matchScoring.recordCard(80, CardType.RED, player1, fixture1);
    List<Card> cards = player1.getCards();

    assertEquals(cards.size(), 1);
    assertEquals(cards.get(0).getCardType(), CardType.RED);
  }

  @Test
  public void testRecordCardOfTypeRedSetsThePlayersEndTime() {
    matchScoring.addPlayerStartTime(0, player1, fixture1);
    matchScoring.recordCard(26, CardType.RED, player1, fixture1);
    PlayerGametime gametime = player1.getMinutesPerGame().get(fixture1);
    assertEquals(gametime.getEndTime(), 26);
  }

  @Test
  public void testRecordCardTwoYellowCardsResultsInARedCard() {
    matchScoring.addPlayerStartTime(0, player1, fixture1);
    matchScoring.recordCard(26, CardType.YELLOW, player1, fixture1);
    matchScoring.recordCard(80, CardType.YELLOW, player1, fixture1);
    List<Card> cards = player1.getCards();
    Card firstCard = cards.get(0);
    Card secondCard = cards.get(1);

    assertEquals(player1.getCards().size(), 2);
    assertEquals(firstCard.getCardType(), CardType.YELLOW);
    assertEquals(secondCard.getCardType(), CardType.RED);
  }

  @Test
  public void testRecordCardTwoYellowCardsSetsPlayerEndTime() {
    matchScoring.addPlayerStartTime(0, player1, fixture1);
    matchScoring.recordCard(26, CardType.YELLOW, player1, fixture1);
    matchScoring.recordCard(80, CardType.YELLOW, player1, fixture1);

    PlayerGametime gametime = player1.getMinutesPerGame().get(fixture1);
    assertEquals(gametime.getEndTime(), 80);
  }

  @Test
  public void testAddPlayerStartTime() {
    matchScoring.addPlayerStartTime(5, player1, fixture1);
    PlayerGametime gametime = player1.getMinutesPerGame().get(fixture1);
    assertEquals(gametime.getStartTime(), 5);
  }

  @Test
  public void testAddPlayerEndTime() {
    matchScoring.addPlayerStartTime(2, player1, fixture1);
    matchScoring.addPlayerEndTime(80, player1, fixture1);

    PlayerGametime gametime = player1.getMinutesPerGame().get(fixture1);
    assertEquals(gametime.getStartTime(), 2);
    assertEquals(gametime.getEndTime(), 80);
  }

  @Test
  public void testRecordSaveWithGoalkeeper() {
    FootballPlayer goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
    matchScoring.recordSave(8, goalkeeper, fixture1);

    assertEquals(goalkeeper.getSaves().size(), 1);
  }

  @Test
  public void testRecordSaveWithOutfieldPlayerRaisesException() {
    assertThrows(IllegalArgumentException.class, () -> {
      matchScoring.recordSave(8, player1, fixture1);
    });
  }

  @Test
  public void testRecordThreeSavesCreatesASaveHatrick() {
    FootballPlayer goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
    matchScoring.recordSave(8, goalkeeper, fixture1);
    matchScoring.recordSave(10, goalkeeper, fixture1);
    matchScoring.recordSave(25, goalkeeper, fixture1);

    assertEquals(goalkeeper.getSaveHatricks().size(), 1);
  }

  @Test
  public void testRecordFiveSavesOnlyCreatesASingleSaveHatrick() {
    FootballPlayer goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
    matchScoring.recordSave(8, goalkeeper, fixture1);
    matchScoring.recordSave(10, goalkeeper, fixture1);
    matchScoring.recordSave(25, goalkeeper, fixture1);
    matchScoring.recordSave(63, goalkeeper, fixture1);
    matchScoring.recordSave(85, goalkeeper, fixture1);

    assertEquals(goalkeeper.getSaveHatricks().size(), 1);
  }

  @Test
  public void testRecordSixSavesCreatesTwoSaveHatricks() {
    FootballPlayer goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
    matchScoring.recordSave(8, goalkeeper, fixture1);
    matchScoring.recordSave(10, goalkeeper, fixture1);
    matchScoring.recordSave(25, goalkeeper, fixture1);
    matchScoring.recordSave(63, goalkeeper, fixture1);
    matchScoring.recordSave(85, goalkeeper, fixture1);
    matchScoring.recordSave(90, goalkeeper, fixture1);

    assertEquals(goalkeeper.getSaveHatricks().size(), 2);
  }
}
