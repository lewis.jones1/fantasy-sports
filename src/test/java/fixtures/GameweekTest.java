package fixtures;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sportsteam.Team;

public class GameweekTest {

  Gameweek gameweek1;
  Gameweek gameweek2;

  FootballFixture fixture1;
  FootballFixture fixture2;
  FootballFixture fixture3;
  FootballFixture fixture4;

  Team team1;
  Team team2;
  Team team3;
  Team team4;

  @BeforeEach
  public void init() {
    gameweek1 = new Gameweek(1, 1, null, null);
    gameweek2 = new Gameweek(2, 2, null, null);

    team1 = new Team(null, null);
    team2 = new Team(null, null);
    team3 = new Team(null, null);
    team4 = new Team(null, null);

    fixture1 = new FootballFixture(1, null, team1, team2);
    fixture2 = new FootballFixture(2, null, team3, team4);
    fixture3 = new FootballFixture(3, null, team2, team1);
    fixture4 = new FootballFixture(4, null, team4, team3);
  }

  @Test
  public void testAddFixtureToGameweek() {
    gameweek1.addFixtureToGameweek(fixture1);

    FootballFixture fixture = gameweek1.getFixtures().get(0);

    assertEquals(gameweek1.getFixtures().size(), 1);
    assertEquals(fixture.getHomeTeam(), team1);
    assertEquals(fixture.getAwayTeam(), team2);
  }

  @Test
  public void testAddMultipleFixturesToSingleGameweek() {
    gameweek1.addFixtureToGameweek(fixture1);
    gameweek1.addFixtureToGameweek(fixture2);

    FootballFixture fixture1 = gameweek1.getFixtures().get(0);
    FootballFixture fixture2 = gameweek1.getFixtures().get(1);

    assertEquals(gameweek1.getFixtures().size(), 2);
    assertEquals(fixture1.getHomeTeam(), team1);
    assertEquals(fixture1.getAwayTeam(), team2);
    assertEquals(fixture2.getHomeTeam(), team3);
    assertEquals(fixture2.getAwayTeam(), team4);
  }

  @Test
  public void testPopulateMultipleGameweeksWithMultipleFixtures() {
    gameweek1.addFixtureToGameweek(fixture1);
    gameweek1.addFixtureToGameweek(fixture2);

    gameweek2.addFixtureToGameweek(fixture3);
    gameweek2.addFixtureToGameweek(fixture4);

    assertEquals(gameweek1.getFixtures().size(), 2);
    assertEquals(gameweek2.getFixtures().size(), 2);
  }
}
