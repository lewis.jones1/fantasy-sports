package pointsscoring.football;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class SaveHatrickTest {
  FootballPlayer forward;
  FootballPlayer midfielder;
  FootballPlayer defender;
  FootballPlayer goalkeeper;

  @BeforeEach
  public void init() {
    forward = new FootballPlayer(null, null, null, Position.FORWARD);
    midfielder = new FootballPlayer(null, null, null, Position.MIDFIELDER);
    defender = new FootballPlayer(null, null, null, Position.DEFENDER);
    goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
  }

  @Test
  public void testGetPointsGoalkeeper() {
    SaveHatrick saveHatrick = new SaveHatrick(goalkeeper, null);
    assertEquals(saveHatrick.getPoints(), 1);
  }

  @Test
  public void testInstantiationWithForwardThrowsException() {
    assertThrows(IllegalArgumentException.class, () -> {
      SaveHatrick saveHatrick = new SaveHatrick(forward, null);
    });
  }

  @Test
  public void testInstantiationWithMidfielderThrowsException() {
    assertThrows(IllegalArgumentException.class, () -> {
      SaveHatrick saveHatrick = new SaveHatrick(midfielder, null);
    });
  }

  @Test
  public void testInstantiationWithDefenderThrowsException() {
    assertThrows(IllegalArgumentException.class, () -> {
      SaveHatrick saveHatrick = new SaveHatrick(defender, null);
    });
  }
}
