package pointsscoring.football;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class AssistTest {

  FootballPlayer forward;
  FootballPlayer midfielder;
  FootballPlayer defender;
  FootballPlayer goalkeeper;

  @BeforeEach
  public void init() {
    forward = new FootballPlayer(null, null, null, Position.FORWARD);
    midfielder = new FootballPlayer(null, null, null, Position.MIDFIELDER);
    defender = new FootballPlayer(null, null, null, Position.DEFENDER);
    goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
  }

  @Test
  public void testGetPointsForward() {
    Assist assist = new Assist(forward, null, 0);
    assertEquals(assist.getPoints(), 3);
  }

  @Test
  public void testGetPointsMidfielder() {
    Assist assist = new Assist(midfielder, null, 0);
    assertEquals(assist.getPoints(), 3);
  }

  @Test
  public void testGetPointsDefender() {
    Assist assist = new Assist(defender, null, 0);
    assertEquals(assist.getPoints(), 3);
  }

  @Test
  public void testGetPointsGoalkeeper() {
    Assist assist = new Assist(goalkeeper, null, 0);
    assertEquals(assist.getPoints(), 3);
  }
}
