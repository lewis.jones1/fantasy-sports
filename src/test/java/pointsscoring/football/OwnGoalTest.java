package pointsscoring.football;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class OwnGoalTest {

  FootballPlayer forward;
  FootballPlayer midfielder;
  FootballPlayer defender;
  FootballPlayer goalkeeper;

  @BeforeEach
  public void init() {
    forward = new FootballPlayer(null, null, null, Position.FORWARD);
    midfielder = new FootballPlayer(null, null, null, Position.MIDFIELDER);
    defender = new FootballPlayer(null, null, null, Position.DEFENDER);
    goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
  }

  @Test
  public void testGetPointsForward() {
    OwnGoal ownGoal = new OwnGoal(forward, null, 0);
    assertEquals(ownGoal.getPoints(), -2);
  }

  @Test
  public void testGetPointsMidfielder() {
    OwnGoal ownGoal = new OwnGoal(midfielder, null, 0);
    assertEquals(ownGoal.getPoints(), -2);
  }

  @Test
  public void testGetPointsDefender() {
    OwnGoal ownGoal = new OwnGoal(defender, null, 0);
    assertEquals(ownGoal.getPoints(), -2);
  }

  @Test
  public void testGetPointsGoalkeeper() {
    OwnGoal ownGoal = new OwnGoal(goalkeeper, null, 0);
    assertEquals(ownGoal.getPoints(), -2);
  }
}
