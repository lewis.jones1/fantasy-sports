package pointsscoring.football;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class GoalTest {

  FootballPlayer forward;
  FootballPlayer midfielder;
  FootballPlayer defender;
  FootballPlayer goalkeeper;

  @BeforeEach
  public void init() {
    forward = new FootballPlayer(null, null, null, Position.FORWARD);
    midfielder = new FootballPlayer(null, null, null, Position.MIDFIELDER);
    defender = new FootballPlayer(null, null, null, Position.DEFENDER);
    goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
  }

  @Test
  public void testGetPointsForward() {
    Goal goal = new Goal(forward, null, 0);
    assertEquals(goal.getPoints(), 4);
  }

  @Test
  public void testGetPointsMidfielder() {
    Goal goal = new Goal(midfielder, null, 0);
    assertEquals(goal.getPoints(), 5);
  }

  @Test
  public void testGetPointsDefender() {
    Goal goal = new Goal(defender, null, 0);
    assertEquals(goal.getPoints(), 6);
  }

  @Test
  public void testGetPointsGoalkeeper() {
    Goal goal = new Goal(goalkeeper, null, 0);
    assertEquals(goal.getPoints(), 6);
  }
}
