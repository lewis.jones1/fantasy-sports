package pointsscoring.football;

import static org.junit.jupiter.api.Assertions.*;

import fixtures.FootballFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class PlayerGametimeTest {
  FootballPlayer player;
  FootballFixture fixture;

  @BeforeEach
  public void init() {
    player = new FootballPlayer(null, null, null, Position.FORWARD);
    fixture = new FootballFixture(1, null, null, null);
  }

  @Test
  public void testGetPoints40MinutesPlayedReturns1Point() {
    PlayerGametime gametime = new PlayerGametime(player, fixture, 0);
    gametime.setEndTime(40);
    assertEquals(gametime.getPoints(), 1);
  }

  @Test
  public void testGetPoints60MinutesPlayedReturns2Points() {
    PlayerGametime gametime = new PlayerGametime(player, fixture, 0);
    gametime.setEndTime(60);
    assertEquals(gametime.getPoints(), 2);
  }

  @Test
  public void testGetPoints0MinutesPlayedReturns0Points() {
    PlayerGametime gametime = new PlayerGametime(player, fixture, 0);
    gametime.setEndTime(0);
    assertEquals(gametime.getPoints(), 0);
  }
}
