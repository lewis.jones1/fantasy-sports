package pointsscoring.football;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class YellowCardTest {
  FootballPlayer forward;
  FootballPlayer midfielder;
  FootballPlayer defender;
  FootballPlayer goalkeeper;

  @BeforeEach
  public void init() {
    forward = new FootballPlayer(null, null, null, Position.FORWARD);
    midfielder = new FootballPlayer(null, null, null, Position.MIDFIELDER);
    defender = new FootballPlayer(null, null, null, Position.DEFENDER);
    goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
  }

  @Test
  public void testGetPointsForward() {
    YellowCard yellowCard = new YellowCard(forward, null, 0);
    assertEquals(yellowCard.getPoints(), -1);
  }

  @Test
  public void testGetPointsMidfielder() {
    YellowCard yellowCard = new YellowCard(midfielder, null, 0);
    assertEquals(yellowCard.getPoints(), -1);
  }

  @Test
  public void testGetPointsDefender() {
    YellowCard yellowCard = new YellowCard(defender, null, 0);
    assertEquals(yellowCard.getPoints(), -1);
  }

  @Test
  public void testGetPointsGoalkeeper() {
    YellowCard yellowCard = new YellowCard(goalkeeper, null, 0);
    assertEquals(yellowCard.getPoints(), -1);
  }

  @Test
  public void testYellowCardHasTypeYellow() {
    YellowCard yellowCard = new YellowCard(goalkeeper, null, 0);
    assertEquals(yellowCard.getCardType(), CardType.YELLOW);
  }
}
