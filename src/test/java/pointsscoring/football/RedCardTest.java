package pointsscoring.football;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class RedCardTest {
  FootballPlayer forward;
  FootballPlayer midfielder;
  FootballPlayer defender;
  FootballPlayer goalkeeper;

  @BeforeEach
  public void init() {
    forward = new FootballPlayer(null, null, null, Position.FORWARD);
    midfielder = new FootballPlayer(null, null, null, Position.MIDFIELDER);
    defender = new FootballPlayer(null, null, null, Position.DEFENDER);
    goalkeeper = new FootballPlayer(null, null, null, Position.GOALKEEPER);
  }

  @Test
  public void testGetPointsForward() {
    RedCard redCard = new RedCard(forward, null, 0);
    assertEquals(redCard.getPoints(), -3);
  }

  @Test
  public void testGetPointsMidfielder() {
    RedCard redCard = new RedCard(midfielder, null, 0);
    assertEquals(redCard.getPoints(), -3);
  }

  @Test
  public void testGetPointsDefender() {
    RedCard redCard = new RedCard(defender, null, 0);
    assertEquals(redCard.getPoints(), -3);
  }

  @Test
  public void testGetPointsGoalkeeper() {
    RedCard redCard = new RedCard(goalkeeper, null, 0);
    assertEquals(redCard.getPoints(), -3);
  }

  @Test
  public void testRedCardHasTypeYellow() {
    RedCard redCard = new RedCard(goalkeeper, null, 0);
    assertEquals(redCard.getCardType(), CardType.RED);
  }
}
