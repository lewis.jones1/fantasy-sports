package steps;

import fixtures.FootballFixture;
import fixtures.FootballMatchScoring;
import io.cucumber.java.en.Given;
import sportsteam.FootballPlayer;
import sportsteam.Position;
import sportsteam.Team;

public class StepDefs {

  FootballPlayer salah;
  FootballFixture fixture1;

  @Given("the tests are set up")
  public void testsAreSetUp() {
    Team liverpool = new Team("LPL", "Liverpool FC");
    Team manCity = new Team("MC", "Manchester City");

    salah = new FootballPlayer("Mohamed", "Salah", liverpool,
        Position.MIDFIELDER);

    fixture1 = new FootballFixture(1, null, liverpool, manCity);
  }

  @Given("{} starts at {int} minutes in {}")
  public void playersPlaysMinutesInFixtures(FootballPlayer player, int startTime, FootballFixture fixture) {
    FootballMatchScoring matchScoring = new FootballMatchScoring();
    matchScoring.addPlayerStartTime(startTime, salah, fixture);
  }
}
