package integration;

import fixtures.FootballFixture;
import fixtures.FootballMatchScoring;
import fixtures.Gameweek;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pointsscoring.football.CardType;
import scoring.PlayerScorer;
import sportsteam.FootballPlayer;
import sportsteam.Position;
import sportsteam.Team;

public class MultiplePlayersGameweeks {
  // teams
  Team liverpool;
  Team manCity;
  Team manUtd;
  Team arsenal;
  Team leicester;
  Team spurs;
  Team brighton;
  Team burnley;
  Team everton;
  Team villa;

  // players
  FootballPlayer salah;
  FootballPlayer trent;
  FootballPlayer deBruyne;
  FootballPlayer foden;
  FootballPlayer fernandes;
  FootballPlayer ronaldo;
  FootballPlayer saka;
  FootballPlayer ramsdale;
  FootballPlayer vardy;
  FootballPlayer iheanacho;
  FootballPlayer kane;
  FootballPlayer son;
  FootballPlayer lamptey;
  FootballPlayer cucurella;
  FootballPlayer mee;
  FootballPlayer tarkowski;
  FootballPlayer richarlison;
  FootballPlayer pickford;
  FootballPlayer coutinho;
  FootballPlayer watkins;

  // fixtures
  FootballFixture fixture1;
  FootballFixture fixture2;
  FootballFixture fixture3;
  FootballFixture fixture4;
  FootballFixture fixture5;
  FootballFixture fixture6;
  FootballFixture fixture7;
  FootballFixture fixture8;
  FootballFixture fixture9;
  FootballFixture fixture10;

  // gameweeks
  Gameweek gameweek1;
  Gameweek gameweek2;

  FootballMatchScoring matchScoring;
  PlayerScorer playerScorer;

  @BeforeEach
  public void setUp() {
    // teams
    liverpool = new Team("LPL", "Liverpool");
    manCity = new Team("MC", "Manchester City");
    manUtd = new Team("MU", "Manchester United");
    arsenal = new Team("ARS", "Arsenal");
    leicester = new Team("LEI", "Leicester");
    spurs = new Team("TH", "Tottenham Hotspur");
    brighton = new Team("BHA", "Brighton and Hove Albion");
    burnley = new Team("BUR", "Burnley");
    everton = new Team("EVE", "Everton");
    villa = new Team("AVL", "Aston Villa");

    // players
    salah = new FootballPlayer("Mohamed", "Salah", liverpool, Position.MIDFIELDER);
    trent = new FootballPlayer("Trent", "Alexander-Arnold", liverpool, Position.DEFENDER);
    deBruyne = new FootballPlayer("Kevin", "De Bruyne", manCity, Position.MIDFIELDER);
    foden = new FootballPlayer("Phil", "Foden", manCity, Position.MIDFIELDER);
    fernandes = new FootballPlayer("Bruno", "Fernandes", manUtd, Position.MIDFIELDER);
    ronaldo = new FootballPlayer("Cristiano", "Ronaldo", manUtd, Position.FORWARD);
    saka = new FootballPlayer("Bukayo", "Saka", arsenal, Position.MIDFIELDER);
    ramsdale = new FootballPlayer("Aaron", "Ramsdale", arsenal, Position.GOALKEEPER);
    vardy = new FootballPlayer("Jamie", "Vardy", leicester, Position.FORWARD);
    iheanacho = new FootballPlayer("Kelechi", "Iheanacho", leicester, Position.FORWARD);
    kane = new FootballPlayer("Harry", "Kane", leicester, Position.FORWARD);
    son = new FootballPlayer("Son", "Heung-Min", leicester, Position.MIDFIELDER);
    lamptey = new FootballPlayer("Tariq", "Lamptey", brighton, Position.DEFENDER);
    cucurella = new FootballPlayer("Marc", "Cucurella", brighton, Position.DEFENDER);
    mee = new FootballPlayer("Ben", "Mee", burnley, Position.DEFENDER);
    tarkowski = new FootballPlayer("James", "Tarkowski", burnley, Position.DEFENDER);
    richarlison = new FootballPlayer(null, "Richarlison", everton, Position.FORWARD);
    pickford = new FootballPlayer("Jordan", "Pickford", everton, Position.GOALKEEPER);
    coutinho = new FootballPlayer("Philip", "Coutinho", villa, Position.MIDFIELDER);
    watkins = new FootballPlayer("Ollie", "Watkins", villa, Position.FORWARD);

    // fixtures
    fixture1 = new FootballFixture(1, null, liverpool, manCity);
    fixture2 = new FootballFixture(2, null, manUtd, arsenal);
    fixture3 = new FootballFixture(3, null, leicester, spurs);
    fixture4 = new FootballFixture(4, null, brighton, burnley);
    fixture5 = new FootballFixture(5, null, everton, villa);
    fixture6 = new FootballFixture(6, null, liverpool, villa);
    fixture7 = new FootballFixture(7, null, manCity, everton);
    fixture8 = new FootballFixture(8, null, manUtd, burnley);
    fixture9 = new FootballFixture(9, null, arsenal, brighton);
    fixture10 = new FootballFixture(10, null, spurs, leicester);

    // gameweeks
    gameweek1 = new Gameweek(1, 1, null, null);
    gameweek2 = new Gameweek(2, 2, null, null);

    // add fixtures to gameweek1
    gameweek1.addFixtureToGameweek(fixture1);
    gameweek1.addFixtureToGameweek(fixture2);
    gameweek1.addFixtureToGameweek(fixture3);
    gameweek1.addFixtureToGameweek(fixture4);
    gameweek1.addFixtureToGameweek(fixture5);

    // add fixtures to gameweek2
    gameweek2.addFixtureToGameweek(fixture6);
    gameweek2.addFixtureToGameweek(fixture7);
    gameweek2.addFixtureToGameweek(fixture8);
    gameweek2.addFixtureToGameweek(fixture9);
    gameweek2.addFixtureToGameweek(fixture10);

    matchScoring = new FootballMatchScoring();
    playerScorer = new PlayerScorer();
  }

  @Test
  public void testSalahScoringTwiceInFirstGameweekAndNoneInSecondGameweek() {
    // gameweek 1
    matchScoring.addPlayerStartTime(0, salah, fixture1);
    matchScoring.addPlayerEndTime(90, salah, fixture1);

    matchScoring.recordGoal(16, salah, null, false, fixture1);
    matchScoring.recordGoal(38, salah, null, false, fixture1);

    matchScoring.recordCard(82, CardType.YELLOW, salah, fixture1);
    assertEquals(playerScorer.calculateGameweekPoints(salah, gameweek1), 11);

    // gameweek 2
    matchScoring.addPlayerStartTime(0, salah, fixture6);
    matchScoring.addPlayerEndTime(90, salah, fixture6);
    assertEquals(playerScorer.calculateGameweekPoints(salah, gameweek2), 2);

    // total
    assertEquals(playerScorer.calculateTotalPoints(salah), 13);
  }
}
