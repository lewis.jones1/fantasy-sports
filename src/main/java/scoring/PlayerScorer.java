package scoring;

import fixtures.FootballFixture;
import fixtures.Gameweek;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import pointsscoring.football.Event;
import pointsscoring.football.PointsScoreable;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class PlayerScorer implements PlayerScorerInterface {

  @Override
  public double calculateTotalPoints(FootballPlayer player) {
    Collection<PointsScoreable> scoreables = getScoringEvents(player);
    return calculatePoints(scoreables);
  }

  @Override
  public double calculateGameweekPoints(FootballPlayer player, Gameweek gameweek) {
    Collection<PointsScoreable> scoreables = getScoringEvents(player, gameweek);
    return calculatePoints(scoreables);
  }

  private double calculatePoints(Collection<PointsScoreable> scoreables) {
    double points;

    points = scoreables
        .stream()
        .mapToInt(PointsScoreable::getPoints)
        .sum();

    return points;
  }

  private Collection<PointsScoreable> getScoringEvents(FootballPlayer player) {
    Collection<PointsScoreable> events = new ArrayList<>();

    events.addAll(player.getMinutesPerGame().values());
    events.addAll(player.getGoals());
    events.addAll(player.getAssists());
    events.addAll(player.getCards());

    if (player.getPosition() == Position.GOALKEEPER) events.addAll(player.getSaveHatricks());

    return events;
  }

  private Collection<PointsScoreable> getScoringEvents(FootballPlayer player, Gameweek gameweek) {
    Collection<PointsScoreable> events = new ArrayList<>();

    events.addAll(
        player.getMinutesPerGame().values()
            .stream()
            .filter((e) -> gameweek.getFixtures().contains(e.getFixture()))
            .collect(Collectors.toList()));

    events.addAll(
        player.getGoals()
            .stream()
            .filter((e) -> gameweek.getFixtures().contains(e.getFixture()))
            .collect(Collectors.toList()));

    events.addAll(
        player.getCards()
            .stream()
            .filter((e) -> gameweek.getFixtures().contains(e.getFixture()))
            .collect(Collectors.toList()));

    return events;
  }
}
