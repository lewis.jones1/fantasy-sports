package scoring;

public interface GameControllerInterface {
  void updateAllGameweekPoints();
  void updateAllValues();
}
