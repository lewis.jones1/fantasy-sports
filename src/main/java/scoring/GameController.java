package scoring;

import java.util.ArrayList;
import java.util.List;
import sportsteam.FootballPlayer;

public abstract class GameController implements GameControllerInterface {
  private static List<FootballPlayer> players = new ArrayList<>();

  private GameController() {
    // to stop instantiation
  }

  public static List<FootballPlayer> getPlayers() {
    return players;
  }

  @Override
  public void updateAllGameweekPoints() {

  }

  @Override
  public void updateAllValues() {

  }
}
