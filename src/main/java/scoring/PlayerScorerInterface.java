package scoring;

import fixtures.Gameweek;
import sportsteam.FootballPlayer;

public interface PlayerScorerInterface {
  // Calculate total points for a player
  double calculateTotalPoints(FootballPlayer player);
  // Calculate points for a player given a gameweek object
  double calculateGameweekPoints(FootballPlayer player, Gameweek gameweek);
}
