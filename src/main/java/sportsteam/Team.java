package sportsteam;

public class Team {
  private int id;
  private String abbreviation;
  private String name;

  public Team(String abbreviation, String name) {
    this.abbreviation = abbreviation;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public String getName() {
    return name;
  }
}
