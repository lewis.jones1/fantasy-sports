package sportsteam;

public enum Position {
  GOALKEEPER, DEFENDER, MIDFIELDER, FORWARD
}
