package sportsteam;

import java.util.ArrayList;
import java.util.List;

public abstract class Member implements Updateable {
  private int id;
  private String firstName;
  private String lastName;
  private Team team;
  private List<Value> valueList;
  private List<Points> pointsList;

  public Member(String firstName, String lastName, Team team) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.team = team;
    this.valueList = new ArrayList<Value>();
    this.pointsList = new ArrayList<Points>();
  }
}
