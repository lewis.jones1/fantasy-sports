package sportsteam;

import fixtures.Fixture;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import pointsscoring.football.Assist;
import pointsscoring.football.Card;
import pointsscoring.football.Goal;
import pointsscoring.football.OwnGoal;
import pointsscoring.football.PlayerGametime;
import pointsscoring.football.Save;
import pointsscoring.football.SaveHatrick;
import scoring.GameController;

public class FootballPlayer extends Member {
  private Position position;
  private String nickname;
  private List<Goal> goals;
  private List<Assist> assists;
  private List<OwnGoal> ownGoals;
  private List<Save> saves;
  private List<SaveHatrick> saveHatricks;
  private List<Card> cards;
  private HashMap<Fixture, PlayerGametime> minutesPerGame;

  public FootballPlayer(String firstName, String lastName, Team team,
      Position position) {
    super(firstName, lastName, team);
    this.position = position;
    this.goals = new ArrayList<>();
    this.assists = new ArrayList<>();
    this.ownGoals = new ArrayList<>();
    this.saves = new ArrayList<>();
    this.saveHatricks = new ArrayList<>();
    this.cards = new ArrayList<>();
    this.minutesPerGame = new HashMap<>();
    GameController.getPlayers().add(this);
  }

  public void updateValue(double value) {

  }

  public void updatePointsList() {

  }

  public Position getPosition() {
    return position;
  }

  public String getNickname() {
    return nickname;
  }

  public List<Goal> getGoals() {
    return goals;
  }

  public List<Save> getSaves() {
    return saves;
  }

  public List<SaveHatrick> getSaveHatricks() {
    return saveHatricks;
  }

  public List<Card> getCards() {
    return cards;
  }

  public List<Assist> getAssists() {
    return assists;
  }

  public List<OwnGoal> getOwnGoals() {
    return ownGoals;
  }

  public HashMap<Fixture, PlayerGametime> getMinutesPerGame() {
    return minutesPerGame;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }
}
