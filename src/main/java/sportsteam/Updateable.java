package sportsteam;

public interface Updateable {
  void updateValue(double value);
  void updatePointsList();
}
