package fixtures;

import pointsscoring.football.CardType;
import sportsteam.FootballPlayer;

public interface EventRecordable {
  void addPlayerStartTime(int startTime, FootballPlayer player, FootballFixture fixture);
  void addPlayerEndTime(int endTime, FootballPlayer player, FootballFixture fixture);
  void recordGoal(int time, FootballPlayer scorer, FootballPlayer assister, boolean ownGoal, FootballFixture fixture);
  void recordCard(int time, CardType cardType, FootballPlayer player, FootballFixture fixture);
  void recordSave(int time, FootballPlayer player, FootballFixture fixture);
  void recordResult(int homeTeamScore, int awayTeamScore, FootballFixture fixture);
}
