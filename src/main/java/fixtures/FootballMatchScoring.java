package fixtures;

import java.util.List;
import pointsscoring.football.Assist;
import pointsscoring.football.Card;
import pointsscoring.football.CardType;
import pointsscoring.football.Goal;
import pointsscoring.football.OwnGoal;
import pointsscoring.football.PlayerGametime;
import pointsscoring.football.RedCard;
import pointsscoring.football.Save;
import pointsscoring.football.SaveHatrick;
import pointsscoring.football.YellowCard;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class FootballMatchScoring implements EventRecordable {

  @Override
  public void addPlayerStartTime(int startTime, FootballPlayer player, FootballFixture fixture) {
    PlayerGametime gametime = new PlayerGametime(player, fixture, startTime);
    player.getMinutesPerGame().put(fixture, gametime);
  }

  @Override
  public void addPlayerEndTime(int endTime, FootballPlayer player, FootballFixture fixture) {
    PlayerGametime gametime = player.getMinutesPerGame().get(fixture);
    gametime.setEndTime(endTime);
  }

  @Override
  public void recordGoal(int time, FootballPlayer scorer, FootballPlayer assister, boolean ownGoal,
      FootballFixture fixture) {

    if (ownGoal) {
      addOwnGoal(time, scorer, fixture);
    } else {
      addGoal(time, scorer, fixture);
    }

    if (assister != null) {
      addAssist(time, assister, fixture);
    }
  }

  @Override
  public void recordCard(int time, CardType cardType, FootballPlayer player,
      FootballFixture fixture) {
    Card card;

    if (playerHasYellowCardInFixture(player, fixture)) {
      card = new RedCard(player, fixture, time);
      setEndTimeIfRedCard(player, fixture, time);
    } else {
      card = createCardBasedOnCardType(cardType, player, fixture, time);
    }

    player.getCards().add(card);
  }

  @Override
  public void recordSave(int time, FootballPlayer player, FootballFixture fixture) {
    if (player.getPosition() != Position.GOALKEEPER) throw new IllegalArgumentException("Must be a goalkeeper");

    Save save = new Save(player, fixture, time);

    List<Save> saves = player.getSaves();

    saves.add(save);

    if (saves.size() % 3 == 0) {
      generateSaveHatrick(player, fixture);
    }
  }

  @Override
  public void recordResult(int homeTeamScore, int awayTeamScore, FootballFixture fixture) {

  }

  private void generateSaveHatrick(FootballPlayer player, FootballFixture fixture) {
    SaveHatrick saveHatrick = new SaveHatrick(player, fixture);
    player.getSaveHatricks().add(saveHatrick);
  }

  private void addOwnGoal(int time, FootballPlayer scorer, FootballFixture fixture) {
    OwnGoal goal = new OwnGoal(scorer, fixture, time);
    scorer.getOwnGoals().add(goal);
  }

  private void addAssist(int time, FootballPlayer assister, FootballFixture fixture) {
    Assist assist = new Assist(assister, fixture, time);
    assister.getAssists().add(assist);
  }

  private void addGoal(int time, FootballPlayer scorer, FootballFixture fixture) {
    Goal goal = new Goal(scorer, fixture, time);
    scorer.getGoals().add(goal);
  }

  private boolean playerHasYellowCardInFixture(FootballPlayer player, FootballFixture fixture) {
    return
        player
            .getCards()
            .stream()
            .anyMatch(
                card1 -> card1.getFixture() == fixture && card1.getCardType() == CardType.YELLOW);
  }

  private Card createCardBasedOnCardType(CardType cardType, FootballPlayer player,
      FootballFixture fixture, int time) {
    Card card = null;

    switch (cardType) {
      case YELLOW:
        card = new YellowCard(player, fixture, time);
        break;
      case RED:
        card = new RedCard(player, fixture, time);
        setEndTimeIfRedCard(player, fixture, time);
        break;
    }
    return card;
  }

  private void setEndTimeIfRedCard(FootballPlayer player, FootballFixture fixture, int time) {
    player.getMinutesPerGame().get(fixture).setEndTime(time);
  }
}
