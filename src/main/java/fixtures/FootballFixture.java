package fixtures;

import java.util.Date;
import sportsteam.Team;

public class FootballFixture extends Fixture {
  private Team homeTeam;
  private Team awayTeam;

  public FootballFixture(int id, Date date, Team homeTeam, Team awayTeam) {
    super(id, date);
    this.homeTeam = homeTeam;
    this.awayTeam = awayTeam;
  }

  public Team getHomeTeam() {
    return homeTeam;
  }

  public Team getAwayTeam() {
    return awayTeam;
  }
}
