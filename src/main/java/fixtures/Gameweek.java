package fixtures;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Gameweek implements GameweekFixture {
  private final int id;
  private int gameweekNumber;
  private Date startDate;
  private Date endDate;
  private List<FootballFixture> fixtures;

  public Gameweek(int id, int gameweekNumber, Date startDate, Date endDate) {
    this.id = id;
    this.gameweekNumber = gameweekNumber;
    this.startDate = startDate;
    this.endDate = endDate;
    this.fixtures = new ArrayList<>();
  }

  public List<FootballFixture> getFixtures() {
    return fixtures;
  }

  public void addFixtureToGameweek(FootballFixture fixture) {
    fixtures.add(fixture);
  }

  public int getId() {
    return id;
  }

  public int getGameweekNumber() {
    return gameweekNumber;
  }

  public void setGameweekNumber(int gameweekNumber) {
    this.gameweekNumber = gameweekNumber;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }
}
