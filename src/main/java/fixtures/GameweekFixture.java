package fixtures;

public interface GameweekFixture {
  void addFixtureToGameweek(FootballFixture fixture);
}
