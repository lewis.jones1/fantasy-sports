package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public abstract class TimedEvent extends Event {
  private int time;

  public TimedEvent(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture);
    this.time = time;
  }

  public int getTime() {
    return time;
  }

  public void setTime(int time) {
    this.time = time;
  }
}
