package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public class PlayerGametime extends Event implements PointsScoreable {
  private int startTime;
  private int endTime;

  public PlayerGametime(FootballPlayer player, FootballFixture fixture, int startTime) {
    super(player, fixture);
    this.startTime = startTime;
  }

  public int getStartTime() {
    return startTime;
  }

  public int getEndTime() {
    return endTime;
  }

  public void setEndTime(int endTime) {
    this.endTime = endTime;
  }

  @Override
  public int getPoints() {
    int points = 0;
    int minutesPlayed = endTime - startTime;

    if (minutesPlayed > 0 && minutesPlayed < 60) {
      points = 1;
    } else if (minutesPlayed >= 60) {
      points = 2;
    }

    return points;
  }
}
