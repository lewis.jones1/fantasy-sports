package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public class YellowCard extends Card{

  public YellowCard(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture, time);
    setCardType(CardType.YELLOW);
  }

  @Override
  public int getPoints() {
    return -1;
  }
}
