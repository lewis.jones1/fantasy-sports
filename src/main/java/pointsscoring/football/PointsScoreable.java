package pointsscoring.football;

public interface PointsScoreable {
  int getPoints();
}
