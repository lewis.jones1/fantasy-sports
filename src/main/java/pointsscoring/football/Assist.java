package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public class Assist extends TimedEvent implements PointsScoreable {

  public Assist(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture, time);
  }

  @Override
  public int getPoints() {
    return 3;
  }
}
