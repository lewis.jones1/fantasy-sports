package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public class OwnGoal extends Goal implements PointsScoreable {

  public OwnGoal(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture, time);
  }

  @Override
  public int getPoints() {
    return -2;
  }
}
