package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public class RedCard extends Card {

  public RedCard(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture, time);
    setCardType(CardType.RED);
  }

  @Override
  public int getPoints() {
    return -3;
  }
}
