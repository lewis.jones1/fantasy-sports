package pointsscoring.football;

import fixtures.FootballFixture;
import java.util.HashMap;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class Goal extends TimedEvent implements PointsScoreable {

  private HashMap<Position, Integer> pointsMap;

  public Goal(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture, time);
    this.pointsMap = new HashMap<>();
    createPointsMap();
  }

  private void createPointsMap() {
    pointsMap.put(Position.GOALKEEPER, 6);
    pointsMap.put(Position.DEFENDER, 6);
    pointsMap.put(Position.MIDFIELDER, 5);
    pointsMap.put(Position.FORWARD, 4);
  }

  @Override
  public int getPoints() {
    Position scorerPosition = this.getPlayer().getPosition();
    return pointsMap.get(scorerPosition);
  }
}
