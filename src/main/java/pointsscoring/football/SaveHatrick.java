package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;
import sportsteam.Position;

public class SaveHatrick extends Event implements PointsScoreable {

  public SaveHatrick(FootballPlayer player, FootballFixture fixture) {
    super(player, fixture);
    if (player.getPosition() != Position.GOALKEEPER) throw new IllegalArgumentException("Must be a goalkeeper");
  }

  @Override
  public int getPoints() {
    return 1;
  }
}
