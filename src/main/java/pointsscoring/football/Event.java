package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public abstract class Event {
  private FootballPlayer player;
  private FootballFixture fixture;

  public Event(FootballPlayer player, FootballFixture fixture) {
    this.player = player;
    this.fixture = fixture;
  }

  public FootballPlayer getPlayer() {
    return player;
  }

  public FootballFixture getFixture() {
    return fixture;
  }

  public void setPlayer(FootballPlayer player) {
    this.player = player;
  }
}
