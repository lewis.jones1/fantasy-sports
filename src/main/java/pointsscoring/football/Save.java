package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public class Save extends TimedEvent {

  public Save(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture, time);
  }
}
