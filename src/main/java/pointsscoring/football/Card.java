package pointsscoring.football;

import fixtures.FootballFixture;
import sportsteam.FootballPlayer;

public abstract class Card extends TimedEvent implements PointsScoreable {
  private CardType cardType;

  public Card(FootballPlayer player, FootballFixture fixture, int time) {
    super(player, fixture, time);
  }

  public CardType getCardType() {
    return cardType;
  }

  public void setCardType(CardType cardType) {
    this.cardType = cardType;
  }
}
